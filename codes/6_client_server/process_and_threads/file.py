
import time


def print1():
    print(1)

def print2():
    #Заглушка какого-либо действия
    #Запрос к БД, расчеты и т.п.
    time.sleep(3)
    print(2)

def print3():
    print(3)

def main():
    print1()
    print2()
    print3()

main()