"""
Простой сервер, принимающий сообщение
из браузера и отправляющий ответ
В браузере (клиент) набрать http://127.0.0.1:2000/request
Работает в цикле, обработка исключений, файлы веб-страниц
"""
import socket


HOST = '127.0.0.1'
PORT = 2000

#1 Вывод логики создания ответа в отедельную функцию
def load_page_from_get_request(request_data):
    HDRS = 'HTTP/1.1 200 OK\r\nContent-Type: text/html; charset=utf-8\r\n\r\n'
    path = request_data.split(' ')[1]
    response = ''
    try:
        with open('views' + path, 'rb') as file:
            response = file.read()
        return HDRS.encode('utf-8') + response
    except FileNotFoundError:
        HDRS_404 = 'HTTP/1.1 404 OK\r\nContent-Type: text/html; charset=utf-8\r\n\r\n'
        return(HDRS_404 + 'Sorry, no page...').encode('utf-8')

#2 Глобальная обработка исключений
try:
    server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    server.bind((HOST, PORT))
    server.listen(4)
    #Нововведение - бесконечный цикл
    while True:
        print('Working...')
        client_socket, address = server.accept()
        data = client_socket.recv(1024).decode('utf-8')
        #print(data)
        content = load_page_from_get_request(data)
        client_socket.send(content)
        #Закрыть соединение для освобождения места другим клиентам
        client_socket.shutdown(socket.SHUT_WR)
except KeyboardInterrupt:
    #закрыть серверный сокет
    server.close()
    print('shutdown this server...')

