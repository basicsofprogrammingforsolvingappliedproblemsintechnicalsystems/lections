"""
Простой сервер, принимающий сообщение
из браузера и отправляющий ответ
В браузере (клиент) набрать http://127.0.0.1:2000/request
"""
import socket

#Адрес-порт сервера
HOST = '127.0.0.1'
PORT = 2000 #принято четырехзначное число назначать

#1. Создание сокета с семейством адресов INET и типом STREAM
server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
#2. Обозначаем сокет как серверный, присвоив ему адрес
server.bind((HOST, PORT))
#Аналог 1-2
#server = socket.create_server((IP_ADDRESS, PORT))

#3. Слушает указанный порт, параметр показывает количество
#входящих запросов, которые будут приняты ОС и поставлены в режим ожидания
#а с 5 запроса будет отказ
server.listen(4)
print('Working...')
#4. С помощью accept() принимаются отправленные запросы и разделяются на
# клиента и адрес, с которого пришел запрос. На этом моменте программа фиксируется
# и не идет дальше, пока не придет запрос
client_socket, address = server.accept()
#5. Получаем содержимое запроса с помощью метода recv('размер байт в пакете')
data = client_socket.recv(1024).decode('utf-8')
print(data)
#6. Отправка строки подключившемуся криенту с помощью метода send()
#Для отправки в браузер (и только в него) необходим заголовок html
HDRS = 'HTTP/1.1 200 OK\r\nContent-Type: text/html; charset=utf-8\r\n\r\n'
content = 'Well done, buddy...'.encode('utf-8')
client_socket.send(HDRS.encode('utf-8') + content)
print('shutdown this server...')