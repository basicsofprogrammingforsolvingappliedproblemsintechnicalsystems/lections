#TCP клиент
import socket

#1. Назначение ip-адреса сервера (хоста) и порта
HOST = '127.0.0.1'
PORT = 2000

#2. Создание сокета
sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
#3. Подключение к серверу
sock.connect((HOST,PORT))
#4. Отправка текстового сообщения
sock.send(b'Test message')
#5. Закрытие клиента
sock.close()