import socket
import random
from threading import Thread
from datetime import datetime
from colorama import Fore, init
# инициализация цветов
init()
# установка доступных цветов
colors = [Fore.BLUE, Fore.CYAN, Fore.GREEN, Fore.LIGHTBLACK_EX,
    Fore.LIGHTBLUE_EX, Fore.LIGHTCYAN_EX, Fore.LIGHTGREEN_EX,
    Fore.LIGHTMAGENTA_EX, Fore.LIGHTRED_EX, Fore.LIGHTWHITE_EX,
    Fore.LIGHTYELLOW_EX, Fore.MAGENTA, Fore.RED, Fore.WHITE, Fore.YELLOW
]
# выбрать случайный цвет для клиента
client_color = random.choice(colors)

SERVER_HOST = "127.0.0.1"
SERVER_PORT = 5002
# мы будем использовать это, чтобы разделить имя клиента и сообщение
separator_token = "<SEP>"

s = socket.socket()
print(f"[*] Connecting to {SERVER_HOST}:{SERVER_PORT}...")
s.connect((SERVER_HOST, SERVER_PORT))
print("[+] Connected.")

# запросить у клиента имя
name = input("Enter your name: ")

def listen_for_messages():
    while True:
        message = s.recv(1024).decode()
        print("\n" + message)
# Поток, который прослушивает сообщения этому клиенту, и распечатайте их
t = Thread(target=listen_for_messages)
# сделать демона потоков таким, чтобы он заканчивался всякий раз,
# когда заканчивается основной поток
t.daemon = True
# начать поток
t.start()

while True:
    # входное сообщение, которое мы хотим отправить на сервер
    to_send =  input()
    # способ выхода из программы
    if to_send.lower() == 'q':
        break
    # добавьте дату и время, имя и цвет отправителя
    date_now = datetime.now().strftime('%Y-%m-%d %H:%M:%S')
    to_send = f"{client_color}[{date_now}] {name}{separator_token}{to_send}{Fore.RESET}"
    # наконец, отправка сообщения
    s.send(to_send.encode())
# закрыть сокет
s.close()