import socket
from threading import Thread

SERVER_HOST = "0.0.0.0"
SERVER_PORT = 5002
#мы будем использовать это, чтобы разделить имя клиента и сообщение
separator_token = "<SEP>"
# инициализировать множества сокетов всех подключенных клиентов
client_sockets = set()
s = socket.socket()
s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
s.bind((SERVER_HOST, SERVER_PORT))
s.listen(5)
print(f"[*] Listening as {SERVER_HOST}:{SERVER_PORT}")

def listen_for_client(cs):
    """
    Эта функция продолжает прослушивать сообщение из сокета `cs`.
     Всякий раз, когда сообщение получено, транслирует его
     всем другим подключенным клиентам.
    """
    while True:
        try:
            # продолжаем прослушивать сообщение из сокета `cs`
            msg = cs.recv(1024).decode()
        except Exception as e:
            # клиент больше не подключен
            # удалить его из набора
            print(f"[!] Error: {e}")
            client_sockets.remove(cs)
        else:
            # если получили сообщение, заменить <SEP>
            # токен с ":" для красивой печати
            msg = msg.replace(separator_token, ": ")
        # перебрать все подключенные сокеты
        for client_socket in client_sockets:
            # и отправка сообщения
            client_socket.send(msg.encode())
while True:
    # сервер постоянно прислушивается к новым подключениям
    client_socket, client_address = s.accept()
    print(f"[+] {client_address} connected.")
    # добавление нового подключенного клиента в подключенные сокеты
    client_sockets.add(client_socket)
    # запустить новый поток, который прослушивает сообщения каждого клиента
    t = Thread(target=listen_for_client, args=(client_socket,))
    # сделать демона потоков таким, чтобы он заканчивался всякий раз,
    # когда заканчивается основной поток
    t.daemon = True
    # старт потока
    t.start()

# закрыть клиентские сокеты
for cs in client_sockets:
    cs.close()
# закрыть серверный сокет
s.close()