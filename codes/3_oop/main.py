# This is a sample Python script.
import ShapeAlt


# Press Shift+F10 to execute it or replace it with your code.
# Press Double Shift to search everywhere for classes, files, tool windows, actions, and settings.


def main():
  import Shape
  a = Shape.Point()
  repr(a)                   #вернет: 'Point(0, 0)'
  b = Shape.Point(3, 4)
  str(b)                    #вернет: '(3, 4)'
  b.distance_from_origin()  # вернет: 5.0
  b.x = -19
  str(b)                    #вернет: '(-19, 4)'
  a==b, a!=b                #вернет: (False, True)
  print(a.__eq__(4))
  p = Shape.Point(3, 9)
  repr(p)                 # вернет: 'Point(3, 9)'
  q = eval(p.__module__ + "." + repr(p))
  repr(q)                   # вернет: 'Point(3, 9)'

  p = Shape.Point(28, 45)
  c = Shape.Circle(5, 28, 45)
  p.distance_from_origin()  # вернет: 53.0
  c.distance_from_origin()  # вернет: 53.0

  print(c.radius)                    # вернет: 5

  circle = ShapeAlt.Circle(5, 28, 45)

  print(circle.radius)                    # вернет: 5
  print(circle.edge_distance_from_origin) # вернет: 48.0

  g = 0

if __name__ == '__main__':
    main()

