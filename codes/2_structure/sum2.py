print("Введите целые числа, за каждым из которых следует Enter;"
      " или Ctrl+D или Ctrl+Z, чтобы закончить")

total = 0
count = 0

while True:
    try:
        line = input()
        if line:
            number = int(line)
            total += number
            count += 1
    except ValueError as err:
        print(err)
        continue
    except EOFError:
        break

if count:
    print("count =", count, "total =", total, "mean =", total / count)