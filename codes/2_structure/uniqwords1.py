import string
import sys

sys.argv = ['uniqwords1.py', 'data/file1.txt', 'data/file2.txt']

words = {}
strip = string.whitespace + string.punctuation + string.digits + "\"'"
for filename in sys.argv[1:]:
    for line in open(filename, encoding="utf8"):
        for word in line.lower().split():
            word = word.strip(strip)
            if len(word) > 2:
                words[word] = words.get(word, 0) + 1

for word in sorted(words):
    print("'{0}' встречается {1} раз".format(word, words[word]))

fin = open(filename, encoding="utf8") # для чтения текста
fout = open(filename, "w", encoding="utf8") # для записи текста