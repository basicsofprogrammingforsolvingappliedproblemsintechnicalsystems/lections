leaps = []
for year in range(1900, 1940):
    if (year % 4 == 0 and year % 100 != 0) or (year % 400 == 0):
        leaps.append(year)

#[item for item in iterable]
#[expression for item in iterable if condition]
leaps = [y for y in range(1900, 1940)] #генерация всех лет

leaps = [y for y in range(1900, 1940)
        if (y % 4 == 0 and y % 100 != 0) or (y % 400 == 0)]


S = {7, "veil", 0, -29, ("x", 11), "sun", frozenset({8, 4, 7}), 913}
S2 = set([7, "veil", 0, -29, ("x", 11), "sun", frozenset({8, 4, 7}), 913])


filenames = ["Makefile", "one", "two"]
filenames = set(filenames)
for makefile in {"MAKEFILE", "Makefile", "makefile"}:
    filenames.discard(makefile)

filenames = set(filenames) - {"MAKEFILE", "Makefile", "makefile"}


d = dict({"id": 1948, "name": "Washer", "size": 3})
d2 = dict(id=1948, name="Washer", size=3)
d3 = dict([("id", 1948), ("name", "Washer"), ("size", 3)])
d4 = dict(zip(("id", "name", "size"), (1948, "Washer", 3)))
d5 = {"id": 1948, "name": "Washer", "size": 3}


for item in d.items():
    print(item[0], item[1])

for key, value in d.items():
    print(key, value)

for value in d.values():
    print(value)

for key in d:
    print(key)

for key in d.keys():
    print(key)
g = 0