# This is a sample Python script.

# Press Shift+F10 to execute it or replace it with your code.
# Press Double Shift to search everywhere for classes, files, tool windows, actions, and settings.
import sys

print(sys.argv)

import random
x = random.randint(1, 6)
y = random.choice(["apple", "banana", "cherry", "durian"])

import numpy as np
from matplotlib import pyplot as plt

def get_int(msg):
    while True:
        try:
            i = int(input(msg))
            return i
        except ValueError as err:
            print(err)

age = get_int("введите ваш возраст: ")

def print_hi(name):
    # Use a breakpoint in the code line below to debug your script.
    print(f'Hi, {name}')  # Press Ctrl+F8 to toggle the breakpoint.
    """
    -973
    210624583337114373395836055367340864637790190801098222508621955072
    0
    "Бесконечно требовательный"
    'Печенин Вадим'
    'positively αβγ€÷©'
    ''
    >>> "Трудные Времена"[5]
    'ы'
    >>> int("45")
    45
    >>> str(912)
    '912'
    """

    x = "blue" #создан объект типа str с текстом "blue", создана ссылка с именем x
    #"переменной" x была присвоена строка 'blue'
    y = "green"
    z = x

    print(x, y, z)  # выведет: blue green blue
    z = y
    print(x, y, z)  # выведет: blue green green
    x = z
    print(x, y, z)  # выведет: green green green

    tuple1 = ('Самара', 'Казань', 'Саратов')
    tuple2 = ('один',)
    tuple3 = ()

    list1 = [1, 4, 9, 16, 25, 36, 49]
    list2 = ['alpha', 'delta', 'echo']
    list3 = ['зебра', 49, -879, 'фыва', 200]
    list4 = []

    list = ["зебра", 49, -879, "фыва", 200]
    list.append("море")
    """
    if x:
        print("x is nonzero")

    if lines < 1000:
        print("малый")
    elif lines < 10000:
        print("средний")
    else:
        print("большой")

    while boolean_expression:
        suite

    while True:
        item = get_next_item()
        if not item:
            break
        process_item(item)
    """
    countries = ["Denmark", "Finland", "Norway", "Sweden"]
    for country in countries:
        print(country)

    for letter in "ABCDEFGHIJKLMNOPQRSTUVWXYZ":
        if letter in "AEIOU":
            print(letter, "это гласная")
        else:
            print(letter, "это согласная")
    """
    try:
        try_suite
    except exception1 as variable1:
        exception_suite1
    …
    except exceptionN as variableN:
        exception_suiteN
    """
    s = input("enter an integer: ")
    try:
        i = int(s)
        print("valid integer entered:", i)
    except ValueError as err:
        print(err)

    text = """Строки в тройных кавычках могут включать 'апострофы' и "кавычки"
    без лишних формальностей. Мы можем даже экранировать символ перевода строки \,
    благодаря чему данная конкретная строка будет занимать всего две строки."""

    a = "Здесь 'апострофы' можно не экранировать, а \"кавычки\" придется."
    b = 'Здесь \'апострофы\' придется экранировать, а "кавычки" не обязательно.'

    t = "Это не самый лучший способ объединения двух длинных строк, " + \
        "потому что он основан на использовании неуклюжего экранирования"
    s = ("Это отличный способ объединить две длинные строки, "
         " потому что он основан на конкатенации строковых литералов.")

    #seq[start]
    #seq[start:end]
    #seq[start:end:step]
    s = "Луч света"
    s = s[:3] + "и" + s[3:]
    #>>>s
    #'Лучи света'

    g = 0
# Press the green button in the gutter to run the script.
if __name__ == '__main__':
    print_hi('PyCharm')

# See PyCharm help at https://www.jetbrains.com/help/pycharm/
