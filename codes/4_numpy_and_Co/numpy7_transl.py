import numpy as np


#Бинарные операции для массивов одного размера
a = np.array([0, 1, 2])
b = np.array([5, 5, 5])
print(a + b)
"""[5 6 7]"""

M = np.ones((3, 3))
print(M + a)
"""[[1. 2. 3.]
    [1. 2. 3.]
    [1. 2. 3.]]"""

a = np.arange(3)
b = np.arange(3)[:, np.newaxis]
print(a + b)
"""[[0 1 2]
    [1 2 3]
    [2 3 4]]"""