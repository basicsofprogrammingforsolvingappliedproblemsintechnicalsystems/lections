import matplotlib.pyplot as plt
import numpy as np

x = np.linspace(0, 10, 100)
plt.figure() # Создаем рисунок для графика
# Создаем первую из двух областей графика и задаем текущую ось
plt.subplot(2, 1, 1) # (rows, columns, panel number)
plt.plot(x, np.sin(x))
# Создаем вторую область и задаем текущую ось
plt.subplot(2, 1, 2)
plt.plot(x, np.cos(x))
plt.show()

# Сначала создаем сетку графиков
# ax будет массивом из двух объектов Axes
fig, ax = plt.subplots(2)
# Вызываем метод plot() соответствующего объекта
ax[0].plot(x, np.sin(x))
ax[1].plot(x, np.cos(x))
plt.show()