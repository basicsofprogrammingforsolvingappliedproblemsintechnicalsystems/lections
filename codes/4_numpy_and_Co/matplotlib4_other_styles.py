import matplotlib.pyplot as plt
import numpy as np

plt.style.use('seaborn-whitegrid')

x = np.linspace(0, 10, 1000)
plt.plot(x, np.sin(x))
plt.xlim(-1, 11) #Ограничение оси X
plt.ylim(-1.5, 1.5) #Ограничение оси Y
#Или так
plt.axis([-1, 11, -1.5, 1.5])
#Подогнать границы
plt.axis('tight')
#Равные оси
plt.axis('equal')
#Подписи к осям
plt.xlabel("x")
plt.ylabel("sin(x)")
plt.show()

#Подписи к графикам
plt.plot(x, np.sin(x), '-g', label='sin(x)')
plt.plot(x, np.cos(x), ':b', label='cos(x)')
plt.axis('equal')
plt.legend() #Отобразить имена графиков
plt.show()