import numpy as np
import matplotlib.pyplot as plt
from matplotlib import cm

def rosen(x):
    """The Rosenbrock function"""
    return np.sum(100.0*(x[1:]-x[:-1]**2.0)**2.0
                  + (1-x[:-1])**2.0, axis=0)


def plot3D():
    # Настраиваем 3D график
    fig = plt.figure(figsize=[15, 10])
    ax = fig.gca(projection='3d')

    # Задаем угол обзора
    ax.view_init(45, 30)

    # Создаем данные для графика
    X = np.arange(-2, 2, 0.1)
    Y = np.arange(-1, 3, 0.1)
    X, Y = np.meshgrid(X, Y)
    Z = rosen(np.array([X, Y]))

    # Рисуем поверхность
    surf = ax.plot_surface(X, Y, Z, cmap=cm.coolwarm)
    plt.show()