#Импорт библиотек
import matplotlib as mpl
import matplotlib.pyplot as plt

#настройка стилей
plt.style.use('classic')

#Первая визуализация
import numpy as np
x = np.linspace(0, 10, 100)
fig = plt.figure(figsize=(9,5)) #Создание объекта-полотна
plt.plot(x, np.sin(x))
plt.plot(x, np.cos(x))
plt.show()

#Для проверки поддерживаемых форматов для сохранения нужно ввесли в Python Console
# fig.canvas.get_supported_filetypes()
print(fig.canvas.get_supported_filetypes())
#Сохранение рисунков в файл
fig.savefig('my_figure.png')



