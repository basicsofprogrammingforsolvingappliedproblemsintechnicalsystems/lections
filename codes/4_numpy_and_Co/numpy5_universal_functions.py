from utils import time_of_function
import numpy as np


np.random.seed(0)

@time_of_function
def compute_reciprocals(values):
    output = np.empty(len(values))
    for i in range(len(values)):
        output[i] = 1.0 / values[i]
    return output

@time_of_function
def compute_reciprocals_numpy(values):
    return 1.0 / values

values = np.random.randint(1, 100, size=1000000)

compute_reciprocals(values)
compute_reciprocals_numpy(values)
"""
Время выполнения функции compute_reciprocals: 1.4592011 c
Время выполнения функции compute_reciprocals_numpy: 1.6187 мс
"""
#Операции с универсальными функциями не ограничиваются одномерными массивами,
# они также могут работать и с многомерными
x = np.arange(9).reshape((3, 3))
print(2 ** x)
"""[[  1   2   4]
    [  8  16  32]
    [ 64 128 256]]"""