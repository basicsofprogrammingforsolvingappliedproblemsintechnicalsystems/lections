import time

def time_of_function(function):
    def wrapped(*args):
        start_time = time.perf_counter_ns()
        res = function(*args)
        allTime = time.perf_counter_ns() - start_time
        unit = 'нс'
        if (allTime>=1000000000):
            allTime/=1000000000
            unit = 'c'
        elif (allTime>=1000000)and(allTime<1000000000):
            allTime /= 1000000
            unit = 'мс'
        elif (allTime >= 1000) and (allTime < 1000000):
            allTime /= 1000
            unit = 'мис'
        print("Время выполнения функции " + function.__name__ + ": " + str(allTime) + " " + unit)
        return res
    return wrapped