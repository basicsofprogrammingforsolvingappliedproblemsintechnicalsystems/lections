import numpy as np

np.random.seed(0) # начальное значение для целей воспроизводимости
x1 = np.random.randint(10, size=6) # одномерный массив
x2 = np.random.randint(10, size=(3, 4)) # двумерный массив
x3 = np.random.randint(10, size=(3, 4, 5)) # трехмерный массив

#У каждого из массивов есть атрибуты ndim (размерность),
# shape (размер каждого измерения) и size (общий размер массива):
print("x3 ndim: ", x3.ndim)
print("x3 shape:", x3.shape)
print("x3 size: ", x3.size)
"""x3 ndim: 3
   x3 shape: (3, 4, 5)
   x3 size: 60"""
print("dtype:", x3.dtype)
"""dtype: int64"""
#Другие атрибуты включают itemsize, выводящий размер (в байтах) каждого
# элемента массива, и nbytes, выводящий полный размер массива (в байтах):
print("itemsize:", x3.itemsize, "bytes")
print("nbytes:", x3.nbytes, "bytes")
"""itemsize: 4 bytes
   nbytes: 240 bytes"""
#В общем значение атрибута nbytes должно быть равно значению атрибута itemsize,
#умноженному на size.