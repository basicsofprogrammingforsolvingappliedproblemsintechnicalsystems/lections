import numpy as np


rng = np.random.RandomState(0)
x = rng.randint(10, size=(3, 4))
print(x)
"""[[5 0 3 3]
    [7 9 3 5]
    [2 4 7 6]]"""

print(x < 6)
"""[[ True  True  True  True]
    [False False  True  True]
    [ True  True False False]]"""

print(np.count_nonzero(x < 6))
"""8"""
print(np.sum(x < 6))
"""8"""
# Сколько значений меньше 6 содержится в каждой строке?
print(np.sum(x < 6, axis=1))
"""[4 2 2]"""
 # Имеются ли в массиве какие-либо значения, превышающие 8?
print(np.any(x > 8))
"""True"""
# Все ли значения меньше 10?
print(np.all(x < 10))
"""True"""
#Булевы операции как маски
print(x[x < 5])
"""[0 3 3 3 2 4]"""


