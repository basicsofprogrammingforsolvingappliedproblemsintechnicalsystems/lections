import matplotlib.pyplot as plt
import numpy as np

plt.style.use('seaborn-whitegrid')
fig1 = plt.figure()
ax1 = plt.axes()
x = np.linspace(0, 10, 1000)
plt.plot(x, np.sin(x - 0), color='blue') # Задаем цвет по названию
plt.plot(x, np.sin(x - 1), color='g') # Краткий код цвета (rgbcmyk)
plt.plot(x, np.sin(x - 2), color='0.75') # Шкала оттенков серого цвета,
# значения в диапазоне от 0 до 1
plt.plot(x, np.sin(x - 3), color='#FFDD44') # 16-ричный код
# (RRGGBB от 00 до FF)
plt.plot(x, np.sin(x - 4), color=(1.0,0.2,0.3)) # Кортеж RGB, значения 0 и 1
plt.plot(x, np.sin(x - 5), color='chartreuse') # Поддерживаются все названия
# цветов HTML
plt.show()

fig2 = plt.figure()
ax2 = plt.axes()
plt.plot(x, x + 0, linestyle='solid') # сплошная линия
plt.plot(x, x + 1, linestyle='dashed') # штриховая линия
plt.plot(x, x + 2, linestyle='dashdot') # штрихпунктирная линия
plt.plot(x, x + 3, linestyle='dotted') # пунктирная линия
# Можно использовать и следующие сокращенные коды:
plt.plot(x, x + 4, linestyle='-') # сплошная линия
plt.plot(x, x + 5, linestyle='--') # штриховая линия
plt.plot(x, x + 6, linestyle='-.') # штрихпунктирная линия
plt.plot(x, x + 7, linestyle=':') # пунктирная линия

plt.plot(x, x + 8, '-g') # сплошная линия зеленого цвета
plt.plot(x, x + 9, '--c') # штриховая линия голубого цвета
plt.plot(x, x + 10, '-.k') # штрихпунктирная линия черного цвета
plt.plot(x, x + 11, ':r') # пунктирная линия красного цвета
plt.show()