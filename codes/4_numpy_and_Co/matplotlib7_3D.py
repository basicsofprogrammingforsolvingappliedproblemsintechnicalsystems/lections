import matplotlib.pyplot as plt
import numpy as np
plt.style.use('seaborn-whitegrid')

ax = plt.axes(projection='3d')
# Данные для трехмерной кривой
zline = np.linspace(0, 15, 1000)
xline = np.sin(zline)
yline = np.cos(zline)
ax.plot3D(xline, yline, zline, 'gray')
# Данные для трехмерных точек
zdata = 15 * np.random.random(100)
xdata = np.sin(zdata) + 0.1 * np.random.randn(100)
ydata = np.cos(zdata) + 0.1 * np.random.randn(100)
ax.scatter3D(xdata, ydata, zdata,
             c=zdata, cmap='Greens')
plt.show()

#Трехмерные поверхностные и каркасные графики
def f(x, y):
    return np.sin(np.sqrt(x ** 2 + y ** 2))

x = np.linspace(-6, 6, 30)
y = np.linspace(-6, 6, 30)
X, Y = np.meshgrid(x, y) #Создание матриц из массивов
Z = f(X, Y)
fig = plt.figure()
ax = plt.axes(projection='3d')
ax.plot_surface(X, Y, Z, rstride=1, cstride=1,
cmap='viridis', edgecolor='none')
ax.set_title('Поверхность')
ax.set_xlabel('x')
ax.set_ylabel('y')
ax.set_zlabel('z')
plt.show()

ax1 = plt.axes(projection='3d')
ax1.plot_wireframe(X, Y, Z, color='black')
ax1.set_title('Каркас')
plt.show()